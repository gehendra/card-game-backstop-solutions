/**
 * Created by gehendrakarmacharya on 6/5/17.
 */
'use strict';
import {Card} from './card';

export const SUITS = ["Diamonds", "Clubs", "Hearts", "Spades"];
export const RANKS = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];

export class Deck {
  constructor() {
      this.cards = [];
      this.init();
  }

  // Main driver function of the deck
  init() {
    this.setDeck(); // Creates a new set of deck
    this.cards = this.shuffle(); // shuffles the deck right away to avoid easy guessing game
  }

  /* New set of deck before shuffle */
  setDeck() {
    SUITS.forEach((suit) => {
      RANKS.forEach((rank) => {
        this.cards.push(new Card(suit, rank));
      });
    });
  }

  getCards() {
    return this.cards;
  }

  /* Shuffle the deck so the players can't guess cards */
  shuffle() {
    let copyOfCards = this.getCards();
    let loopThroughTotalNumberOfCards = copyOfCards.length;
    let temp;
    let selectedSpot;
    let i;

    while(loopThroughTotalNumberOfCards) {
      selectedSpot = Math.floor(Math.random() * loopThroughTotalNumberOfCards--); // Select random array index from the array
      [copyOfCards[loopThroughTotalNumberOfCards], copyOfCards[selectedSpot]] = [copyOfCards[selectedSpot], copyOfCards[loopThroughTotalNumberOfCards]]; // Swap it with last element of the array
    }
    return copyOfCards;
  }

  // This function returns card object which has bigger value than just true or false for further evaluation.
  compareTwoCards(cardOne, cardTwo) {
    if (cardOne instanceof Card === true && cardTwo instanceof Card === true) {
      if (this.checkCardEquivalentNumber(cardOne.getValue()) == this.checkCardEquivalentNumber(cardTwo.getValue())) {
        return  0;
      }
      else if (this.checkCardEquivalentNumber(cardOne.getValue()) > this.checkCardEquivalentNumber(cardTwo.getValue())) {
        return 1;
      }
      else {
        return -1;
      }
    }
    else
      return false;
  }

  /* This function pick card(s) from the deck passed to it. second parameter determines number of cards to draw from the deck.
    * Third parameter lets user to pick any position from the deck
  */
  drawCard(cards = this.getFirstDeck(), n = 1, index = 0) {
    if(cards.length >= n + index) {
      cards.splice(index, n);
    }
    else {
      console.log('Deck does not have that many cards');
    }    
    return cards;
  }

  /**
    * This function cut current deck into two deck base on random number generated.
    * Also, the selected random index should not be 0 to avoid getting only one card for the first deck or getting all cards from the deck leaving nothing for second deck.
  */
  getCutPositionForSplittingDeck(deck = this.cards, index = 0) {
    let selectedIndex = 0;
    if (deck.length > 0) { // Check to ensure deck is not empty
      do {
        selectedIndex = Math.floor(Math.random() * deck.length)
      } while( selectedIndex == 0 || selectedIndex == deck.length - 1)
    }
    return selectedIndex;
  }

  checkCardEquivalentNumber(cardValue) {
    let value = cardValue;
    switch (cardValue) {
      case 'A':
        cardValue = 14;
        break;
      case 'K':
        cardValue = 13;
        break;
      case 'Q':
        cardValue = 12;
        break;
      case 'J':
        cardValue = 11;
        break;
      default:
        cardValue = +cardValue // convert into number
    }
    return cardValue;
  }
}
