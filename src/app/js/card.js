/**
 * Created by gehendrakarmacharya on 6/5/17.
 */

'use strict';
const CARD_WITH_LONG_NAME = {
   A: 'Ace',
   J: 'Jack',
   Q: 'Queen',
   K: 'King'
 };

export class Card {
  constructor(suite, value) {
    this.suite = suite;
    this.value = value;
  }

  getSuite() {
    return this.suite;
  }

  getValue() {
    return this.value;
  }

  getValueAndSuite() {
    switch (this.value) {
      case 'A' || 'a':
        return `${CARD_WITH_LONG_NAME['A']} of ${this.suite}`
        break;
      case 'K' || 'k':
        return `${CARD_WITH_LONG_NAME['K']} of ${this.suite}`
        break;
      case 'Q' || 'q':
        return `${CARD_WITH_LONG_NAME['Q']} of ${this.suite}`
        break;
      case 'J' || 'j':
        return `${CARD_WITH_LONG_NAME['J']} of ${this.suite}`
        break;
      default:
        return `${this.value} of ${this.suite}`
        break;
    }
  }
}
