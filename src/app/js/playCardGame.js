/**
 * Created by gehendrakarmacharya on 6/5/17.
 */

import {Deck} from './deck';
/*
  * This class is a driver class that performs game.
*/
export class CardGame {
  constructor() {
    this.deck = new Deck();
  }

  cutDeck() {
    this.currentDeck = this.deck.getCards();

    if (this.currentDeck.length > 0) {
      this.firstDeck = this.currentDeck.splice(0, this.deck.getCutPositionForSplittingDeck(this.currentDeck));
      this.secondDeck = this.currentDeck.slice();
      this.currentDeck= '';
    }
  }

  getFirstDeck() {
    return this.firstDeck;
  }

  getSecondDeck() {
    return this.secondDeck;
  }

  playGame() {
    this.cutDeck();
    let smallestDeck = this.firstDeck.length > this.secondDeck.length ? this.secondDeck : this.firstDeck;
    var deckOneScore = 0;
    var deckTwoScore = 0;
    let firstDeck = this.getFirstDeck();
    let secondDeck = this.getSecondDeck();
    let count = 0;

    for( let i = 0; i < smallestDeck.length; i++) {
      let result = +this.deck.compareTwoCards(this.drawCardFromCurrentDeck(firstDeck[i]), this.drawCardFromCurrentDeck(secondDeck[i]));
      if (result == 1)  {
          deckOneScore += 1;
      }
      else if (result == -1) {
        deckTwoScore += 1;
      }
      count += 1;
    }
    let result = `Deck One => ${deckOneScore}, Deck Two => ${deckTwoScore} with total round of ${count}`;
    if (deckOneScore == deckTwoScore) {
      result += ' and it is a Tie game...';
    }
    else if (deckOneScore > deckTwoScore) {
      result += ', Deck One won the game...';
    }
    else {
      result += ', Deck two won the game...';
    }
    console.log(result);
  }

  /* This function pick card(s) from the deck passed to it. second parameter determines number of cards to draw from the deck.
    * Third parameter lets user to pick any position from the deck
  */

  drawCardFromCurrentDeck(cardsDeck = this.getFirstDeck()) {
    if (cardsDeck !== "") {
      return cardsDeck;
    }
  }
}
