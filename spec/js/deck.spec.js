import { expect } from 'chai';

import {Card} from '../../src/app/js/card';
import {Deck} from '../../src/app/js/deck';

let deck = new Deck();

describe("Deck", () => {
    context("when is created", () => {
        it("should not be empty", () => {
          expect(deck.getCards()).to.be.not.empty;
        });

        it("should have 52 cards on the deck", () => {
          expect(deck.getCards().length).to.equal(52);
        });

        it("should be able to suffle", () => {
          let deckBeforeShuffle = deck.getCards().slice(0); // making a copy of the deck before shuffle
          let deckAfterShuffle = deck.shuffle();
          //console.log(`${JSON.stringify(deckBeforeShuffle)} => ${deckBeforeShuffle.length}`);
          //console.log(`${JSON.stringify(deckAfterShuffle)} => ${deckAfterShuffle.length}`);
          expect(JSON.stringify(deckAfterShuffle)).to.not.equal(JSON.stringify(deckBeforeShuffle)); // converting both deck to json stringy and comparing than comparing objects
        });
    });

    context("should be able to draw cards", () => {
      it("should be able to draw a card from the top", ()=> {
        let deckBeforeDraw = deck.getCards().slice(0); // making a copy of the deck before draw card(s) from it
        console.log('....draw method....');
        console.log(`${JSON.stringify(deckBeforeDraw)} => ${deckBeforeDraw.length}`);
        deck.drawCard(deck.getCards());
        console.log(`${JSON.stringify(deck.getCards())} => ${deck.getCards().length}`);
        expect(deck.getCards().length).to.equal(deckBeforeDraw.length - 1);
      });

      it("should be able to draw card(s) from the top", ()=> {
        let deckBeforeDraw = deck.getCards().slice(0); // making a copy of the deck before draw card(s) from it
        console.log('....draw method 5 cards....');
        console.log(`${JSON.stringify(deckBeforeDraw)} => ${deckBeforeDraw.length}`);
        deck.drawCard(deck.getCards(), 5); // draw 5 cards
        console.log(`${JSON.stringify(deck.getCards())} => ${deck.getCards().length}`);
        expect(deck.getCards().length).to.equal(deckBeforeDraw.length - 5);
      });
    });
});
