import { expect } from 'chai';

import {Card} from '../../src/app/js/card';

var card = new Card('Diamonds', 'A')
describe("Card", () => {
    context("when created", () => {
        it("should not be empty value", () => {
          expect(card.getValue()).to.be.not.empty;
        });
        it("should not be empty suite", () => {
          expect(card.getSuite()).to.be.not.empty;
        });
        it("should have a value", () => {
          expect(card.getValue()).to.equal('A');
        });
        it("should have a suite", () => {
          expect(card.getSuite()).to.equal('Diamonds');
        });
        it("should have a method to display value and suite", () => {
          expect(card.getValueAndSuite()).to.equal('Ace of Diamonds');
        });
    });
});
